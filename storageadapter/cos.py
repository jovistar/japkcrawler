# -*- coding: utf-8 -*-
from qcloud_cos import CosConfig
from qcloud_cos import CosS3Client

def storage(param, path, name):
    cosConfig = CosConfig(Secret_id=param['SECRET_ID'], Secret_key=param['SECRET_KEY'], Region=param['REGION'])
    cosClient = CosS3Client(cosConfig)

    response = cosClient.upload_file(
        Bucket=param['BUCKET'],
        LocalFilePath=path,
        Key=name,
        PartSize=10,
        MAXThread=5
    )

    return True