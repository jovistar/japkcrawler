# -*- coding: utf-8 -*-

#redis
REDIS_HOST              = ''
REDIS_PORT              = 6379
REDIS_DB_BROKER         = 1
REDIS_DB_BACKEND        = 2
REDIS_DB_CHANNEL        = 0
REDIS_CHANNEL_OBJECT    = 'object'
REDIS_CHANNEL_PAGE      = 'page'
REDIS_CHANNEL_SHUTDOWN  = 'shutdown'

SEED_PAGE = {}
SEED_PAGE['anzhispider']= ('http://www.anzhi.com/tsort_2709_49_1_hot.html', )

DOWNLOADER_PATH = 'tmp'

STORAGE_COS_SECRET_ID = ''
STORAGE_COS_SECRET_KEY= ''
STORAGE_COS_REGION = 'ap-shanghai'
STORAGE_COS_BUCKET = ''