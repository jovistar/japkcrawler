# -*- coding: utf-8 -*-
import util
import os
import pycurl

def download(url, path):
    if util.file_exists(path):
        os.remove(path)

    curlHandle = pycurl.Curl()
    curlHandle.setopt(pycurl.FOLLOWLOCATION, 1)
    curlHandle.setopt(pycurl.MAXREDIRS, 5)
    curlHandle.setopt(pycurl.CONNECTTIMEOUT, 60)
    curlHandle.setopt(pycurl.TIMEOUT, 300)
    curlHandle.setopt(pycurl.NOSIGNAL, 1)

    downFile = open(path, 'wb')
    curlHandle.setopt(pycurl.WRITEDATA, downFile)
    curlHandle.setopt(pycurl.URL, url)
    try:
        curlHandle.perform()
        downFile.close()
    except:
        return False

    if 0 == util.file_size(path):
        return False
    return True