# -*- coding: utf-8 -*-
import os
import hashlib
from datetime import *

def enable_path(path):
	if not os.path.exists(path):
		os.makedirs(path)

def gen_uid(seed):
    m = hashlib.md5()
    
    curTime = datetime.now()
    input = curTime.strftime('%Y-%m-%d %H:%M:%S') + seed
    m.update(input.encode('utf-8'))
    return m.hexdigest()

def file_size(path):
	return os.path.getsize(path)

def file_exists(path):
    return os.path.exists(path)