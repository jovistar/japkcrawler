# -*- coding: utf-8 -*-
import os
from celery import Celery
import settings
import util
from downloader import download
from storageadapter.cos import storage


celeryBroker = 'redis://%s:%d/%d' % (settings.REDIS_HOST, settings.REDIS_PORT, settings.REDIS_DB_BROKER)
workerApp = Celery('worker', broker=celeryBroker)

@workerApp.task
def do_crawl(spiderName, seedPage):
    cwd = os.getcwd()
    os.chdir(cwd + '/crawler')
    os.system('scrapy crawl %s -a seedPage=%s > /dev/null 2>&1' % ( spiderName, seedPage))
    os.chdir(cwd)

@workerApp.task
def do_download(objectUrl):
    util.enable_path(settings.DOWNLOADER_PATH)

    uid = util.gen_uid(objectUrl)
    fileName = uid + '.apk'
    filePath = settings.DOWNLOADER_PATH + '/' + fileName

    if not download(objectUrl, filePath):
        return

    print('%s (%s) is Downloaded.'  % (objectUrl, fileName))

    storageParam = {}
    storageParam['SECRET_ID'] = settings.STORAGE_COS_SECRET_ID
    storageParam['SECRET_KEY']= settings.STORAGE_COS_SECRET_KEY
    storageParam['REGION'] = settings.STORAGE_COS_REGION
    storageParam['BUCKET'] = settings.STORAGE_COS_BUCKET

    if storage(storageParam, filePath, fileName):
       print('%s is Stored.' % fileName)

    os.remove(filePath) 