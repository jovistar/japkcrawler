# -*- coding: utf-8 -*-

import scrapy
from crawler.items import ObjectItem
from crawler.items import PageItem

class AnzhiSpider(scrapy.Spider):
    name = 'anzhispider'
    start_urls = []

    def __init__(self, seedPage=''):
        self.start_urls.append(seedPage)

    def parse(self, response):
        # object url
        for objectId in response.xpath('//div[@class="app_down"]/a[@href="javascript:void(0)"]/@onclick').extract():
            objectItem = ObjectItem()
            objectItem['url'] = 'http://www.anzhi.com/dl_app.php?s=%s&n=5' % objectId[9: -2]
            yield objectItem

        for pageUrl in response.xpath('//div[@class="pagebars"]/a/@href').extract():
            pageItem = PageItem()
            pageItem['spider'] = self.name
            pageItem['url'] = 'http://www.anzhi.com' + pageUrl
            yield pageItem

        