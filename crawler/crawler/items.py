# -*- coding: utf-8 -*-

import scrapy

class PageItem(scrapy.Item):
    spider = scrapy.Field()
    url = scrapy.Field()

class ObjectItem(scrapy.Item):
    url = scrapy.Field()
