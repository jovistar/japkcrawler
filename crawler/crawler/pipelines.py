# -*- coding: utf-8 -*-
from crawler.items import ObjectItem
from crawler.items import PageItem

import crawler.settings as settings
import redis

class ItemPipeline(object):
    redisConnection = None

    def __init__(self):
        pass

    def process_item(self, item, spider):
        self.init_redis()
        
        if isinstance(item, ObjectItem):
            self.redisConnection.publish(settings.REDIS_CHANNEL_OBJECT, item['url'])
        elif isinstance(item, PageItem):
            newItem = {}
            newItem['spider'] = item['spider']
            newItem['url'] = item['url']
            self.redisConnection.publish(settings.REDIS_CHANNEL_PAGE, newItem)
        else:
            pass

        return item

    def init_redis(self):
        if None != self.redisConnection:
            return

        self.redisConnection = redis.StrictRedis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.REDIS_DB_CHANNEL)
