# -*- coding: utf-8 -*-
import settings
import redis
from worker import do_crawl
from worker import do_download

from ast import literal_eval
import signal

# global
g_object_urls_downloaded = []
g_page_url_crawled = []

rc = None
ps = None

def init_mq():
    rc = redis.StrictRedis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.REDIS_DB_CHANNEL, decode_responses=True)
    
    if None == rc:
        return None, None

    ps = rc.pubsub()
    ps.subscribe([settings.REDIS_CHANNEL_OBJECT, settings.REDIS_CHANNEL_PAGE, settings.REDIS_CHANNEL_SHUTDOWN])
    return rc, ps

def init_crawling():
    global g_page_url_crawled

    for spider in settings.SEED_PAGE.keys():
        for seedPage in settings.SEED_PAGE[spider]:
            print('Distributing %s' % seedPage)
            do_crawl.delay(spider, seedPage)
            g_page_url_crawled.append(seedPage)

def process_object(objectUrl):
    global g_object_urls_downloaded

    if objectUrl in g_object_urls_downloaded:
        return

    if len(g_object_urls_downloaded) > 5:
        return

    print('Downloading %s' % objectUrl)
    do_download.delay(objectUrl)
    g_object_urls_downloaded.append(objectUrl)

def process_page(page):
    global g_page_url_crawled

    if page['url'] in g_page_url_crawled:
        return
    
    if len(g_page_url_crawled) > 0:
        return

    print('Distributing %s' % page['url'])
    do_crawl.delay(page['spider'], page['url'])
    g_page_url_crawled.append(page['url'])

def signal_handler(sig, frame):
    global rc

    rc.publish(settings.REDIS_CHANNEL_SHUTDOWN, 'shutdown')
    print('\nWarm shutdown (SchedulerProcess)')

def main():
    global rc
    global ps

    rc, ps = init_mq()
    if None == rc or None == ps:
        print('ERROR')
        return

    signal.signal(signal.SIGTERM, signal_handler)
    signal.signal(signal.SIGINT, signal_handler)

    init_crawling()

    for item in ps.listen():
        if 'message' != item['type']:
            continue

        if settings.REDIS_CHANNEL_SHUTDOWN == item['channel']:
            break
        elif settings.REDIS_CHANNEL_OBJECT == item['channel']:
            process_object(item['data'])
        elif settings.REDIS_CHANNEL_PAGE == item['channel']:
            process_page(literal_eval(item['data']))
        else:
            continue
    
    print("Shutting Down")

if __name__ == '__main__':
    main()